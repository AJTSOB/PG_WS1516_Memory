// BoS1.cpp : Definiert den Einstiegspunkt fuer die Konsolenanwendung.
//

#include "stdafx.h"
#include <time.h>
#include <random>

// Speichert den Wert und den Status eines Feldes
struct Feld
{
	int zahl;
	bool aufgedeckt;
};

// Zum Einstellen der Feldgr��e in den Optionen
enum Groesse {
	Winzig = 4,
	Klein = 16,
	Mittel = 24,
	Gross = 36
};

// Aktueller Spielzustand
enum Status
{
	Optionen,
	Initialisieren,
	Zug,
	Pruefen,
	Spielende
};

// Variablen
int colors[] = { 0xFF0000, 0x880000, 0x00ff00, 0x008800, 0x0000ff, 0x000088, 0x00FFFF, 0x008888, 0xFF00FF, 0x880088, 0xFFFF00, 0x888800, 0x00FF88, 0x0088FF, 0xFF0088, 0x8800ff, 0xff8800, 0x88ff00 };
int spielerZug = 0;
Feld feld[36] = {};
int auswahl[2] = {};
int punkte[2] = {};
int stelle = 0;
char *nachricht = "";
char t[100] = {};

Status status = Optionen;
Groesse g = Klein;

// Zeigt Optionen zum Einstellen der Spielfeldgr��e im BoS-Fenster
void OptionenAnzeigen()
{
	formen("s");
	groesse(4, 1);
	text(0, "Winzig");
	text(1, "Klein");
	text(2, "Mittel");
	text(3, "Gross");
}

// F�llt das Feldarray mit Standartwerten
void FeldErstellen(Feld array[], int size)
{
	for (int i = 0; i < size; i++)
	{
		array[i] = { i / 2, false };
	}
}

// Mischt die Felder
void Mischen(Feld array[], int size)
{
	for (int i = 0; i < size - 1; i++)
	{
		Feld tmp = array[i];
		int r = rand() % size;
		array[i] = array[r];
		array[r] = tmp;
	}
}

// Stellt die Anzahl der Felder im BoS-Fenster ein
void FeldGroesse(Groesse g)
{
	switch (g)
	{
	case Winzig:
		groesse(2, 2);
		break;
	case Klein:
		groesse(4, 4);
		break;
	case Mittel:
		groesse(6, 4);
		break;
	case Gross:
		groesse(6, 6);
		break;
	default:
		break;
	}

	for (int i = 0; i < g; i++)
		symbolGroesse(i, .49);
}

// �berpr�ft, ob bereits alle Felder aufgedeckt wurden
bool SpielEnde(Feld array[], int size)
{
	for (int i = 0; i < size; i++)
	{
		if (!array[i].aufgedeckt) return false;
	}
	return true;
}

// Setzt einige Variablen auf ihre Standardwerte zur�ck
void Reset()
{
	for (int i = 0; i < g; i++)
	{
		feld[i].aufgedeckt = false;
 	}
	punkte[0] = 0;
	punkte[1] = 0;
	spielerZug = 0;
}

// Diese Funktion startet das Spiel
void Memory()
{
	// BoS-Feld l�schen und Optionen Anzeigen
	loeschen();
	OptionenAnzeigen();

	// Gameloop
	while (true)
	{
		// F�hrt je nach Spielzustand Code aus
		switch (status)
		{
		case Optionen: // Spieleinstellungen
			nachricht = abfragen();

			if (strlen(nachricht) > 0 && nachricht[0] == '#')
			{
				int x = 0;
				sscanf_s(nachricht, "# %d", &x);
				g = (x == 0) ? Winzig : (x == 1) ? Klein : (x == 2) ? Mittel : Gross;
				status = Initialisieren;
			}
			else Sleep(50);
			
			break;
		case Initialisieren: // F�hrt Funktionen zum Initialisieren des Spiels aus
			FeldErstellen(feld, g);
			Mischen(feld, g);
			FeldGroesse(g);
			Reset();
			formen("s");
			farben(0xBBBBBB);
			status = Zug;
			break;
		case Zug: // L�sst den Spieler 2 zugedeckte Felder ausw�hlen
			sprintf_s(t, "Spieler %d ist an der Reihe", spielerZug+1);
			statusText(t);
			nachricht = abfragen();
			if (strlen(nachricht) > 0 && nachricht[0] == '#')
			{
				int x = 0;
				sscanf_s(nachricht, "# %d", &x);
				if (!feld[x].aufgedeckt)
				{
					feld[x].aufgedeckt = true;
					farbe(x, colors[feld[x].zahl]);
					auswahl[stelle++] = x;
					if (stelle > 1)
						status = Pruefen;
				}

			}
			else Sleep(50);
			break;
		case Pruefen: // Pr�ft, ob die beiden ausgew�hlten Felder den gleichen Wert haben
			stelle = 0;
			if (feld[auswahl[0]].zahl == feld[auswahl[1]].zahl) // Ziehendem Spieler einen Punkt hinzuf�gen
			{
				punkte[spielerZug] ++;
			}
			else // Spieler wechseln
			{

				Sleep(2500);
				for (int i = 0; i < 2; i++)
				{
					feld[auswahl[i]].aufgedeckt = false;
					farbe(auswahl[i], 0xBBBBBB);
				}
				spielerZug = (spielerZug + 1) % 2;
			}
			if (SpielEnde(feld, g))
				status = Spielende;
			else
				status = Zug;
			break;
		case Spielende: // Zeigt an, dass das Spiel vorbei ist und schreibt den Spielstand in die Statusnachricht
			sprintf_s(t, "Spiel vorbei! Spieler 1: %d Punkte  |  Spieler 2: %d Punkte", punkte[0], punkte[1]);
			statusText(t);
			Sleep(5000);
			OptionenAnzeigen();
			status = Optionen;
			break;
		default:
			break;
		}

	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));

	Memory();
	
	getchar();
    return 0;
}