THM - PG Projekt für Wintersemester 15/16

Projektname: Memory

Inhalt/Funktionalität:
Implementierung eines Memoryspiels für 2-4 Leute in der Programmiersprache C. Die grafische Ausgabe erfolgt über Board of Symbols.

Größte technische Herausforderung und Lösungsansatz:
Die größte technische Herausforderung wird die Programmierung des Zustandssystems sein. Eine mögliche Lösung wären Zustände als Integer-Zahlen darzustellen, und über eine switch-case Kontrollstruktur auszuwerten.